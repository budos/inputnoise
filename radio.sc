Server.default.waitForBoot({
/*
	goal:
	reload the sample every time the python script sends a completion message.
	Then feed the sample to a GrainBuf synthDef.

	steps:
	1. make synth def
*/
// SynthDef(\grain, {|amp = 0.5, trigFreq = 100, grainDur = 0.1,  filePos = 0.5, rate = 1, pan = 0|
// 	var src;
// 	src = GrainBuf.ar(1, Impulse.kr(trigFreq), SinOsc.kr(0.01)*grainDur, b.bufnum, rate, SinOsc.kr(0.1, mul: 0.43, add: filePos), pan, c.bufnum)!2;
// 	Out.ar(0, src * amp);
// }).add;
Server.default.waitForBoot({
	(
		// reverb
		SynthDef(\rev, {|amp = 0.2, mix = 0.44, room = 0.33, damp = 0.55|
			var reverb;
			reverb = FreeVerb.ar(In.ar(129, 2), mix, room, damp)!2;
			Out.ar(0, reverb);
		}).play;

		//
		~counter= true;
		b = Buffer.read(s, "/var/log/sc/noise.wav", bufnum: 1);
		d = Buffer.read(s, "/var/log/sc/noise.wav", bufnum: 2);
		c = Buffer.sendCollection(s, Env.sine().discretize);
		SynthDef(\grain, {|amp = 0.3, trigFreq = 10, grainDur = 0.5,  filePos = 0.5, rate = 1, pan = 0, phasorRate = 1, gate = 1 , bufnum = 1|
			var src, src2, src3, src4, env, envgen;
			env = Env.asr(attackTime: 100, releaseTime: 150);
			envgen = EnvGen.kr(env, gate: gate, doneAction: 2);
			src = GrainBuf.ar(1, Impulse.kr(trigFreq), SinOsc.kr(0.01, mul:0.4, add: 0.5)*grainDur, bufnum, rate,SinOsc.kr(0.00066, mul: 0.5, add: 0.5), pan)!2;
			src2 = GrainBuf.ar(1, Impulse.kr(trigFreq), SinOsc.kr(0.03, mul:0.4, add: 0.5)*grainDur, bufnum, rate*9/8, SinOsc.kr(0.066, mul: 0.5, add: 0.5), pan)!2;
			src3 = GrainBuf.ar(1, Impulse.kr(trigFreq), SinOsc.kr(0.03, mul:0.4, add: 0.5)*grainDur, bufnum, rate*11/10, SinOsc.kr(0.066, mul: 0.5, add: 0.5), pan)!2;
			src4 = GrainBuf.ar(1, Impulse.kr(trigFreq), SinOsc.kr(0.03, mul:0.4, add: 0.5)*grainDur, bufnum, rate*3/4, SinOsc.kr(0.066, mul: 0.5, add: 0.5), pan)!2;
			src = src/2 + src2/2 + src3/2 + src4/2;
			Out.ar(129, Compander.ar(src, src,1, 0.5, 0.01, 0.01) * amp * envgen);
			"grain ADDED".postln;
		}).add;
		SynthDef(\grain2, {|amp = 0.3, trigFreq = 400, grainDur = 0.4,  filePos = 0.5, rate = 1, pan = 0, phasorRate = 0.5, gate = 1 , bufnum = 2|
			var src, src2, env, envgen;
			env = Env.asr(attackTime: 10, releaseTime: 10);
			envgen = EnvGen.kr(env, gate: gate, doneAction: 2);
			src = GrainBuf.ar(1, Impulse.kr(trigFreq), SinOsc.kr(0.1, mul:0.3, add: 0.5)*grainDur, bufnum, rate, SinOsc.kr(0.0066, mul: 0.5, add: 0.5), pan)!2;
			src2 = GrainBuf.ar(1, Impulse.kr(trigFreq/2), grainDur, bufnum, rate*2/3,	SinOsc.kr(0.00066, mul: 0.5, add: 0.5), pan)!2;
			src = src + src2/2;
			Out.ar(129, Compander.ar(src, src,1, 0.5, 0.01, 0.01) * amp * envgen);
			"grain ADDED 2".postln;
		}).add;

		OSCdef.new(\reciever,{| msg, time, addr, port|
			if(~counter,{
				j.set(\gate, 0);
				d.free;
				d = Buffer.read(s, "/var/log/sc/noise.wav", bufnum: 2);
				~counter.postln;
				~counter=false;
				k = Synth(\grain2,[\grainDur, rrand(0.030,0.01),\rate, rrand(2.0, 1.0), \trigFreq, rrand(1000.0, 100.0), \bufnum, 2]);
			},
				{
					k.set(\gate, 0);
					b.free;
					b = Buffer.read(s, "/var/log/sc/noise.wav", bufnum: 1);
					~counter.postln;
					~counter=true;
					j = Synth(\grain,[\grainDur, rrand(0.5,0.02),\rate, rrand(1.0, 0.7), \trigfreq, rrand(1000.0,100.0), \bufnum, 1]);
				}
			);
			// b = Buffer.read(s, "/var/log/sc/noise.wav", bufnum: 1);
			// j = Synth(\grain,[\grainDur, 1.0.linrand,\rate, 1.0.rand, \phasorRate, 1.0.rand]);
			msg.postln;
		},'/reloadSample', recvPort: 6666);
	)
});
});
